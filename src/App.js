import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';
import AxiosLibrary from "./component/axiosLibrary";
import FetchApi from "./component/fetchApi";

function App() {
  return (

    <div className="container text-center mt-5">
      <div className="row justify-content-center">
        <div className="col-6">
          <FetchApi></FetchApi>
          <hr></hr>
          <AxiosLibrary></AxiosLibrary>
        </div>
      </div>
    </div>
  );
}

export default App;
