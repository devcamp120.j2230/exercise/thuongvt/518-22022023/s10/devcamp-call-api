import { Component } from "react";
import axios from "axios";

class AxiosLibrary extends Component {
   AxiosLibrary = async(config)=>{
       
        let response = await axios(config);
        console.log(response.data)

        return response.data

  }

  getByIdhandler = ()=>{
    var config = {
        method: 'get',
        maxBodyLength: Infinity,
        url: 'https://jsonplaceholder.typicode.com/posts/1',
        headers: { }
      };

      this.AxiosLibrary(config)
      .then((response)=>{
          console.log(response) // bắt được return
      })
      .catch((err)=>{
          console.error(err)
      })

  }

  getAllHandler=()=>{
    var config = {
        method: 'get',
        maxBodyLength: Infinity,
        url: 'https://jsonplaceholder.typicode.com/posts',
        headers: { }
      };
      this.AxiosLibrary(config)
      .then((response)=>{
          console.log(response) // bắt được return
      })
      .catch((err)=>{
          console.error(err)
      })

  }

  postHandler=()=>{
    var data = JSON.stringify({
      "userId": 104,
      "id": 1,
      "title": "test",
      "body": "test"
    });
    
    var config = {
      method: 'post',
    maxBodyLength: Infinity,
      url: 'https://jsonplaceholder.typicode.com/posts',
      headers: { 
        'Content-Type': 'application/json'
      },
      data : data
    };
    
      this.AxiosLibrary(config)
      .then((response)=>{
          console.log(response) // bắt được return
      })
      .catch((err)=>{
          console.error(err)
      })
     
  }

  updaterHandler = ()=>{
    var data = JSON.stringify({
        "userId": 104,
        "id": 1,
        "title": "test",
        "body": "test"
      });
      
      var config = {
        method: 'put',
      maxBodyLength: Infinity,
        url: 'https://jsonplaceholder.typicode.com/posts/1',
        headers: { 
          'Content-Type': 'application/json'
        },
        data : data
      };

      this.AxiosLibrary(config)
      .then((response)=>{
          console.log(response) // bắt được return
      })
      .catch((err)=>{
          console.error(err)
      })

  }

  deleteHandler=()=>{
    var config = {
        method: 'delete',
      maxBodyLength: Infinity,
        url: 'https://jsonplaceholder.typicode.com/posts/1',
        headers: { }
      };
    
      this.AxiosLibrary(config)
      .then((response)=>{
          console.log(response) // bắt được return
      })
      .catch((err)=>{
          console.error(err)
      })

  }
  render(){
      return(
          <>
          <div className="row">
              <h4>axios</h4>
          </div>
          <div className="justify-content">
              <div className="form-control mt-3 bg-warning">
                      <button className="btn btn-danger" onClick={this.getAllHandler}>Get all</button>
              </div>
              <div className="form-control mt-3 bg-warning">
                      <button className="btn btn-danger" onClick={this.getByIdhandler}>Get detail</button>
              </div>
              <div className="form-control mt-3 bg-warning">
                      <button className="btn btn-danger" onClick={this.postHandler}>Create</button>
              </div>
              <div className="form-control mt-3 bg-warning">
                      <button className="btn btn-danger" onClick={this.updaterHandler}>Update</button>
              </div>
              <div className="form-control mt-3 bg-warning">
                      <button className="btn btn-danger" onClick={this.deleteHandler}>Delete</button>
              </div>
          </div>
          </>
      )
  }
}

export default AxiosLibrary