import { Component } from "react";

class FetchApi extends Component {
    fetchAPI = async(url,requestOptions)=>{
       
          let response = await fetch(url, requestOptions);
          let data = await response.json();
          console.log(data)

          return data

    }

    getByIdhandler = ()=>{
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
          };
          var url = "https://jsonplaceholder.typicode.com/posts/1"

        this.fetchAPI(url,requestOptions)
        .then((response)=>{
            console.log(response) // bắt được return
        })
        .catch((err)=>{
            console.error(err)
        })

    }

    getAllHandler=()=>{
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
          };
          var url = "https://jsonplaceholder.typicode.com/posts"

        this.fetchAPI(url,requestOptions)
        .then((response)=>{
            console.log(response) // bắt được return
        })
        .catch((err)=>{
            console.error(err)
        })

    }

    postHandler=()=>{
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        
        var raw = JSON.stringify({
          "userId": 201,
          "id": 212,
          "title": "qui est esse",
          "body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
        });
        
        var requestOptions = {
          method: 'POST',
          headers: myHeaders,
          body: raw,
          redirect: 'follow'
        };
        var url = "https://jsonplaceholder.typicode.com/posts"

        this.fetchAPI(url,requestOptions)
        .then((response)=>{
            console.log(response) // bắt được return
        })
        .catch((err)=>{
            console.error(err)
        })
       
    }

    updaterHandler = ()=>{
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        
        var raw = JSON.stringify({
          "userId": 123,
          "id": 212,
          "title": "tets",
          "body": "tets"
        });
        
        var requestOptions = {
          method: 'PUT',
          headers: myHeaders,
          body: raw,
          redirect: 'follow'
        };
        
        var url = "https://jsonplaceholder.typicode.com/posts/1"

        this.fetchAPI(url,requestOptions)
        .then((response)=>{
            console.log(response) // bắt được return
        })
        .catch((err)=>{
            console.error(err)
        })

    }

    deleteHandler=()=>{
        var requestOptions = {
            method: 'DELETE',
            redirect: 'follow'
          };
          
          var url = "https://jsonplaceholder.typicode.com/posts/1"

        this.fetchAPI(url,requestOptions)
        .then((response)=>{
            console.log(response) // bắt được return
        })
        .catch((err)=>{
            console.error(err)
        })

    }
    render(){
        return(
            <>
            <div className="row">
                <h4>Fetch API</h4>
            </div>
            <div className="justify-content">
                <div className="form-control mt-3 bg-warning">
                        <button className="btn btn-danger" onClick={this.getAllHandler}>Get all</button>
                </div>
                <div className="form-control mt-3 bg-warning">
                        <button className="btn btn-danger" onClick={this.getByIdhandler}>Get detail</button>
                </div>
                <div className="form-control mt-3 bg-warning">
                        <button className="btn btn-danger" onClick={this.postHandler}>Create</button>
                </div>
                <div className="form-control mt-3 bg-warning">
                        <button className="btn btn-danger" onClick={this.updaterHandler}>Update</button>
                </div>
                <div className="form-control mt-3 bg-warning">
                        <button className="btn btn-danger" onClick={this.deleteHandler}>Delete</button>
                </div>
            </div>
            </>
        )
    }
}

export default FetchApi